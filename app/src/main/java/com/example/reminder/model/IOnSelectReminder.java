package com.example.reminder.model;

import android.content.Context;

public interface IOnSelectReminder {
    public void onSelectReminder(Reminder reminder, Context context);
}
