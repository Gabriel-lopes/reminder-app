package com.example.reminder.activities;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.example.reminder.R;
import com.example.reminder.database.ReminderRepository;
import com.example.reminder.model.IOnSelectReminder;
import com.example.reminder.model.Reminder;

public class UpdateReminderActivity  extends AppCompatActivity {
    EditText title, date, time;
    Button updateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        Intent intent = getIntent();
        Reminder reminder = (Reminder) intent.getSerializableExtra("reminderSelected");

        title = findViewById(R.id.titleId);
        title.setText(reminder.getTitle());

        date = findViewById(R.id.dateId);
        date.setText(reminder.getDate());

        time = findViewById(R.id.timeId);
        time.setText(reminder.getTime());

        updateButton = findViewById(R.id.updatebutton);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder.setTitle(title.getText().toString());
                reminder.setDate(date.getText().toString());
                reminder.setTime(time.getText().toString());

                ReminderRepository.getInstance(v.getContext()).updateReminder(reminder);

                Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                intentBack.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentBack);
            }
        });
    }
}
